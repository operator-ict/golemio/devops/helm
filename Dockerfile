
ARG HELM_VERSION=3.13.1
FROM alpine/helm:$HELM_VERSION
ARG KUBECTL_VERSION=1.28.3

ARG USER=nonroot
ENV HOME /home/$USER

ENV BUILD_PACKAGES \
  ca-certificates \
  yq \
  curl

# enable edge/community repo [yq]
# RUN echo "@main http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
  
# install packages
RUN \
  apk --no-cache --update-cache --available upgrade && \
  apk add --no-cache --update-cache ${BUILD_PACKAGES} && \
  rm -rf /var/cache/apk/*

WORKDIR /usr/local/bin

# kubectl
RUN curl -sLO https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
    mv kubectl /usr/bin/kubectl && \
    chmod +x /usr/bin/kubectl


# non-root container
RUN adduser -S -D -G root $USER

USER $USER
WORKDIR $HOME

ENTRYPOINT [""]
